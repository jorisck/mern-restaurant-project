import {ObjectId} from "mongodb"

let restaurants // use to save a ref to the db

export default class RestaurantsDAO{
    static async injectDB(conn){ //how we inatialy connect to our DB. We call this method as soon as the server starts
        if (restaurants){
            return // iif the ref is alrady filled we just have a return
        }
        try {
            restaurants = await conn.db(process.env.RESTREVIEWS_NS).collection("restaurants")
        } catch (e) {
            console.error(
                `Unable to establish a collection handle in restaurantsDAO: ${e}`,
            )
        }
    }

    static async getRestaurants({ // to get a list of restaurants
        filters = null, // here the options for this method, just created for it
        page = 0,
        restaurantsPerPage = 20,
    } = {}){
        let query
        if (filters) {
            if ("name" in filters){//query are very powerful in mongodb and the are a lot of things we can do
                query = {$text: {$search: filters["name"]}} //for name there inot a database field, we have to set it up at mongodb atlas: if someone does a text search which of the field of the DB will be search for that specific string
            } else if ("cuisine" in filters){
                query = {"cuisine": {$eq: filters["cuisine"]}}
            } else if ("zipcode" in filters){
                query = {"address.zipcode": {$eq: filters["zipcode"]}}
            }
        }

        let cursor

        try {
            cursor = await restaurants
             .find(query)
        } catch (e) {
            console.error(`Unable to issue find command, ${e}`)
            return {restaurantsList: [], totalNumRestaurants:0}
        }

        const displayCursor = cursor.limit(restaurantsPerPage).skip(restaurantsPerPage*page) //skip from the begenning

        try {
            const restaurantsList = await displayCursor.toArray()
            const totalNumRestaurants = await restaurants.countDocuments(query)

            return {restaurantsList, totalNumRestaurants}
        } catch (e) {
            console.error(
                `Unable to convert cursor to array or problem counting documents, ${e}`
            )
            return {restaurantsList:[], totalNumRestaurants:0}
        }
    }

    static async getRestaurantByID(id) {
        try {
          const pipeline = [
            {
                $match: {
                    _id: new ObjectId(id),
                },
            },
                  {
                      $lookup: {
                          from: "reviews",
                          let: {
                              id: "$_id",
                          },
                          pipeline: [
                              {
                                  $match: {
                                      $expr: {
                                          $eq: ["$restaurant_id", "$$id"],
                                      },
                                  },
                              },
                              {
                                  $sort: {
                                      date: -1,
                                  },
                              },
                          ],
                          as: "reviews",
                      },
                  },
                  {
                      $addFields: {
                          reviews: "$reviews",
                      },
                  },
              ]
          return await restaurants.aggregate(pipeline).next()
        } catch (e) {
          console.error(`Something went wrong in getRestaurantByID: ${e}`)
          throw e
        }
      }
    
      static async getCuisines() {
        let cuisines = []
        try {
          cuisines = await restaurants.distinct("cuisine")
          return cuisines
        } catch (e) {
          console.error(`Unable to get cuisines, ${e}`)
          return cuisines
        }
      }
}