import React from "react";
import { Routes as Switch, Route, Link } from "react-router-dom"; //The React error 'Switch' is not exported from 'react-router-dom' occurs when we try to import 'Switch' from react-router-dom v6. In react-router-dom v6, ‘Switch’ is replaced by ‘Routes’.
import "bootstrap/dist/css/bootstrap.min.css";

import AddReview from "./components/add-review";
import Restaurant from "./components/restaurant";
import RestaurantsList from "./components/restaurants-list";
import Login from "./components/login";
import Navbar from "./components/navbar";

function App() {
  const [user, setUser] = React.useState(null);

  async function login(user = null) {
    setUser(user);
  }

  async function logout() {
    setUser(null)
  }

  return (
    <div>
      <Navbar logout={logout} user={user} />
      
      <div className="container mt-3">
        <Switch>
          <Route exact path="/" element={<RestaurantsList />} />
          <Route exact path="/restaurants" element={<RestaurantsList />} />
          <Route 
            path="/restaurants/:id/review"
            element={<AddReview user={user}/>}
          />
          <Route 
            path="/restaurants/:id"
            element={<Restaurant user={user}/>}
          />
          <Route 
            path="/login"
            element={<Login login={login}/>}
          />
        </Switch>
      </div>
    </div>
  );
}

export default App;